﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseUleam.Almacenamiento
{
    class Menu
    {
        public int Id { get; set; }
        public void Desplegar()
        {
            int opcion1, opcion2, opcion3;


            do
            {
                Console.ForegroundColor = ConsoleColor.Green;

                Console.Clear();
                Console.WriteLine("====================UNIVERSIDAD LAICA ELOY ALFARO DE MANABÍ=================");
                Console.WriteLine("=========FACULTAD DE CIENCIAS INFORMÁTICAS=======TECNOLOGÍAS DE LA INFORMACIÓN======");
                Console.WriteLine("SISTEMA DE CONTROL DE REGISTRO DE TUTORÍAS ACADÉMICAS");
                Console.WriteLine("Bienvenido");
                Console.WriteLine("Como que tipo de Usuario desea ingresar");
                Console.WriteLine("\n 1.- Iniciar Sesion\n" +
                                  "\n 2.- Registrarse\n" +
                                  "\n 3.- Salir \n Opcion:  ");

                opcion1 = Convert.ToInt32(Console.ReadLine());
                switch (opcion1)
                {
                    case 1:
                        do
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.Clear();

                            Console.WriteLine("================================");
                            Console.WriteLine(" 1.- Iniciar Sesión \n" +
                                              " 2.- Regresar");
                            opcion2 = Convert.ToInt32(Console.ReadLine());
                            switch (opcion2)
                            {
                                case 1:


                                      LoginGeneral();
                                    //do
                                    //{

                                    //    Console.Clear();
                                    //    Console.WriteLine("Bienvenido a la ventana de Registro de Usuario");
                                    //    Console.WriteLine("\n ================Elija el Usuario para INICIAR SESION ================\n" +
                                    //                      " 1                   Docente               \n" +
                                    //                      " 2                   Alumno                \n" +
                                    //                      " 3                   Administrador         \n" +
                                    //                      " 4                   Salir                   ");
                                    //    opcion3 = Convert.ToInt32(Console.ReadLine());
                                    //    switch (opcion3)
                                    //    {
                                    //        case 1:
                                    //            Console.Clear();
                                    //            LoginDocente();
                                    //            break;
                                    //        case 2:
                                    //            Console.Clear();
                                    //            LoginEstudiante();
                                    //            break;
                                    //        case 3:
                                    //            Console.Clear();
                                    //            LoginAdministrador();
                                    //            Console.WriteLine("\n Presione la tecla ENTER para continuar \n");
                                    //            break;
                                    //        case 4:
                                    //            break;
                                    //    }
                                    //} while (opcion3 != 4);
                                    break;

                                case 2:
                                    break;
                            }
                        } while (opcion2 != 2);
                        break;

                    case 2:
                        do
                        {
                            Console.ForegroundColor = ConsoleColor.Blue;
                            Console.Clear();
                            Console.WriteLine("Bienvenido a la ventana de Registro de Usuario");
                            Console.WriteLine("\n Elija el Usuario a Registrarse \n" +
                                              " 1.- Docente \n" +
                                              " 2.- Alumno \n" +
                                              " 3.- Administrador \n" +
                                              " 4.- Salir");
                            opcion3 = Convert.ToInt32(Console.ReadLine());
                            switch (opcion3)
                            {
                                case 1:
                                    Console.Clear();
                                    Console.WriteLine("Bienvenido a la ventana de Registro de Usuario Docente");
                                    IngresarProfesor();
                                    Console.WriteLine(" *********** Se ha registrado satisfactoriamente *********** ");
                                    Console.WriteLine("Presione la tecla ENTER para continuar");
                                    //CrearRegistroUsuario();
                                    //IngresarAsignaturas();
                                    //ListaAulas();
                                    break;
                                case 2:

                                    Console.Clear();
                                    Console.WriteLine("Bienvenido a la ventana de Registro de Usuario Alumno");
                                    IngresarAlumno();
                                    Console.WriteLine(" *********** Se ha registrado satisfactoriamente *********** ");
                                    Console.WriteLine("Presione la tecla ENTER para continuar");
                                    break;
                                case 3:
                                    Console.Clear();
                                    Console.WriteLine("Bienvenido a la ventana de Registro de Usuario Administrador");
                                    IngresarAdministrador();
                                 //   Console.WriteLine(" *********** Se ha registrado satisfactoriamente *********** ");
                                    //CrearRegistroUsuario();
                                    //IngresarAulas();
                                    Console.WriteLine("Presione la tecla ENTER para continuar");
                                    break;
                                case 4:
                                    break;
                            }
                        } while (opcion3 != 4);
                        break;

                }
            } while (opcion1 != 3);
        }

        static void ListadoProfesores()
        {
            using (var db = new EntityFrameworkContext())
            {
                List<Docentes> listadoProfesores = db.Docentess.ToList();
                foreach (var profesor in listadoProfesores)
                {
                    Console.WriteLine("Apellido:" + profesor.Apellido + "Nombre" + profesor.Nombre);
                }
            }
        }
        static void ListadoAlumnos()
        {
            using (var db = new EntityFrameworkContext())
            {
                List<Alumnos> listadoAlumnos = db.Alumnoss.ToList();
                foreach (var alumnos in listadoAlumnos)
                {
                    Console.WriteLine("Apellido:" + alumnos.Apellido + "Nombre" + alumnos.Nombre);
                }
            }
        }
        static void IngresarProfesor()
        {
            using (var db = new EntityFrameworkContext())
            {
                Usuario usuario = new Usuario();
                Docentes docentes = new Docentes();
                Console.WriteLine("Ingrese el nombre del docente");
                docentes.Nombre = Console.ReadLine();
                Console.WriteLine("Ingrese el apellido del docente");
                docentes.Apellido = Console.ReadLine();
                Console.WriteLine("Ingrese su numero de cedula");
                docentes.Cedula = Console.ReadLine();
                Console.WriteLine("Su correo es el siguiente");
                docentes.Correo = "p" + docentes.Cedula + "@uleam.edu.ec";
                Console.WriteLine(docentes.Correo);
                Console.WriteLine("Asigne una contraseña a su cuenta");
                docentes.Contrasenia = Console.ReadLine();
                Console.WriteLine("******* Su correo es el siguiente : " + docentes.Correo + " ********** Su contraseña es la siguiente :" + docentes.Contrasenia);
                Console.WriteLine("Presione la tecla ENTER para continuar");
                Console.ReadLine();

                db.Add(docentes);
                db.SaveChanges();

            }
        }
        static void IngresarAdministrador()
        {
            using (var db = new EntityFrameworkContext())
            {
                Usuario usuario = new Usuario();

                Administrador administrador = new Administrador();
                Console.WriteLine("Ingrese el nombre del docente");
                administrador.Nombre = Console.ReadLine();
                Console.WriteLine("Ingrese el apellido del docente");
                administrador.Apellido = Console.ReadLine();
                Console.WriteLine("Ingrese su numero de cedula");
                administrador.Cedula = Console.ReadLine();
                Console.WriteLine("Su correo es el siguiente");
                administrador.Correo = "ad" + administrador.Cedula + "@uleam.edu.ec";
                Console.WriteLine(administrador.Correo);
                Console.WriteLine("Asigne una contraseña a su cuenta");
                administrador.Contrasenia = Console.ReadLine();
                Console.WriteLine("******* Su correo es el siguiente : " + administrador.Correo + " ********** Su contraseña es la siguiente :" + administrador.Contrasenia);
                Console.WriteLine("Presione la tecla ENTER para continuar");
                Console.ReadLine();

                db.Add(administrador);
                db.SaveChanges();

            }
            return;
        }
        static void IngresarAlumno()
        {
            using (var db = new EntityFrameworkContext())
            {
               
                Usuario usuario = new Usuario();
                Alumnos alumnos = new Alumnos();
                Console.WriteLine("Ingrese el nombre ");
                alumnos.Nombre = Console.ReadLine();
                Console.WriteLine("Ingrese el apellido");
                alumnos.Apellido = Console.ReadLine();
                Console.WriteLine("Ingrese su numero de cedula");
                alumnos.Cedula = Console.ReadLine();
                Console.WriteLine("Ingrese su nivel cursado actualmente \n" +
                    " ======== 1 - 2 - 3 - 4 - 5 - 6 - 7 - 8 - 9 - 10 ========");
                alumnos.Nivel = Console.ReadLine();
                Console.WriteLine("Ingrese su matricula (Primera, Segunda o Tercera matricula)");
                alumnos.Matricula = Console.ReadLine();
                Console.WriteLine("Su correo es el siguiente");
                alumnos.Correo = "e" + alumnos.Cedula + "@uleam.edu.ec";              
                Console.WriteLine(alumnos.Correo);
                Console.WriteLine("Asigne una contraseña a su cuenta");
                alumnos.Contrasenia = Console.ReadLine();
                Console.WriteLine(" \n ******* Su correo es el siguiente : " + alumnos.Correo + " ********** Su contraseña es la siguiente :" + alumnos.Contrasenia);
                Console.WriteLine("Presione la tecla ENTER para continuar");
                Console.ReadLine();

                db.Add(alumnos);
                db.SaveChanges();

            }
            return;
        }

        static void ListaAulas()
        {
            using (var db = new EntityFrameworkContext())
            {
                List<Aula> ListaAulas = db.Aulas.ToList();
                foreach (var aulas in ListaAulas)
                {
                    Console.WriteLine(aulas.Id + ".  " + aulas.NombreAula);
                }
            }
        }

        static void IngresarAulas()
        {
            using (var db = new EntityFrameworkContext())
            {
                Aula aula = new Aula();
                Console.WriteLine("Ingrese el nombre del Aula");
                aula.NombreAula = Console.ReadLine();
                Console.WriteLine(aula.NombreAula);

                db.Add(aula);
                db.SaveChanges();

            }
            return;
        }
        static void IngresarAsignaturas()
        {
            using (var db = new EntityFrameworkContext())
            {
                Docentes docentes = new Docentes();
                Console.WriteLine("Ingrese la/las asignatura/s");
                docentes.Asignatura = Console.ReadLine();
                Console.WriteLine(docentes.Asignatura, docentes.Nombre, docentes.Apellido);
                db.Add(docentes);
                db.SaveChanges();
            }
            return;
        }
        static void ListaDeAsignaturas()
        {
            using (var db = new EntityFrameworkContext())
            {
                List<Docentes> ListaDeAsignaturas = db.Docentess.ToList();
                foreach (var Docentes in ListaDeAsignaturas)
                {
                    Console.WriteLine("Asignatura/s: " + Docentes.Asignatura);
                }
            }
        }

        static void CrearTutoria()
        {
            using (var db = new EntityFrameworkContext())
            {
                Docentes docentes = new Docentes();
                Tutoria tutoria = new Tutoria();
                SeleccionarTutoria seleccionarTutoria = new SeleccionarTutoria();

                DateTime fecha = DateTime.Now;
                Console.WriteLine("Proceda a escribir la fecha y hora de la Tutoria");
                Console.WriteLine("Escriba el Año ");
                string anio = Console.ReadLine();
                Console.WriteLine("Escriba el Mes \n" +
                    "=====01--02--03--04--05--06--07--08--09--10--11--12=====");
                string mes = Console.ReadLine();
                Console.WriteLine("Ingrese el Dia \n" +
                   "=====Ingresarlo en números=====");
                string dia = Console.ReadLine();
                Console.WriteLine("Escriba la hora" +
                    "=====Formato 24 Horas=====");
                string hora = Console.ReadLine();
                DateTime crearfecha = new DateTime(int.Parse(anio), int.Parse(mes), int.Parse(dia), int.Parse(hora), 0, 0);
                tutoria.Fecha = crearfecha;
                seleccionarTutoria.Fecha = tutoria.Fecha;

                Console.WriteLine("Ingrese el Aula a utilizar");
                tutoria.Aula = Console.ReadLine();
                seleccionarTutoria.Aula = tutoria.Aula;
                Console.WriteLine("Ingrese la Asignatura");
                tutoria.Asignatura = Console.ReadLine();
                seleccionarTutoria.Asignatura = tutoria.Asignatura;
                Console.WriteLine("Ingrese el Tema");
                tutoria.Tema = Console.ReadLine();
                seleccionarTutoria.Tema = tutoria.Tema;
                Console.WriteLine("\nSe asignó tutoría para:");
                seleccionarTutoria.Docente = docentes.Nombre;
                tutoria.FechaCreacion = DateTime.Now;
                tutoria.Docente = docentes.Apellido + docentes.Nombre;
                Console.WriteLine(tutoria.Fecha + "  " +   tutoria.Aula + "  " +   tutoria.Asignatura + "\n" +   tutoria.Tema + "    Fecha de Creacion " +   tutoria.FechaCreacion + "      " +   tutoria.Docente +   docentes.Apellido +" " +   docentes.Nombre);

                db.Add(tutoria);
                db.SaveChanges();
            }
            return;
        }

        static void ListaDeTutorias()
        {
            using (var db = new EntityFrameworkContext())
            {
                Docentes docentes = new Docentes();
                List<Tutoria> ListaDeTutorias = db.Tutorias.ToList();
                foreach (var Tutoria in ListaDeTutorias)
                {

                    Console.WriteLine("Fecha: " + Tutoria.Fecha + " Aula:  " + Tutoria.Aula + "Asignatura:  " + Tutoria.Asignatura + " Tema:  " + Tutoria.Tema);
                }
            }
        }
        static void ListaDeTutoriasAgregadas()
        {
            using (var db = new EntityFrameworkContext())
            {
                var seleccionarLista = (from resultado in db.SeleccionarTutorias
                                        where resultado.Estudiante == "Nombre"
                                        select resultado).FirstOrDefault<SeleccionarTutoria>();
                List<SeleccionarTutoria> SeleccionDeTutorias = db.SeleccionarTutorias.ToList();
                foreach (var seleccionarTutoria in SeleccionDeTutorias)
                {
                    Console.WriteLine("Fecha: " + seleccionarTutoria.Fecha + " " + seleccionarTutoria.Aula + " " + seleccionarTutoria.Asignatura + " " + seleccionarTutoria.Tema); ;
                }
            }
        }

        static void ListaDeTutoriaestudiante()
        {
            using (var db = new EntityFrameworkContext())
            {
                List<Tutoria> ListaDeTutorias = db.Tutorias.ToList();
                foreach (var Tutoria in ListaDeTutorias)
                {
                    Console.WriteLine( "Fecha: " + Tutoria.Fecha + Tutoria.Aula + "   "+ Tutoria.ID + " .- " +Tutoria.Asignatura + " ====TEMA====  " + Tutoria.Tema + "    DOCENTE :  " + Tutoria.Docente);
                }
            }
        }

        static void SeleccionarTutoria()
        {
            using (var db = new EntityFrameworkContext())
            {
                Alumnos alumnos = new Alumnos();
                SeleccionarTutoria seleccionarTutoria = new SeleccionarTutoria();
                Console.WriteLine("Seleccione la tutoria a la que se quiere integrar");
                var tutoria = (from resultado in db.Tutorias
                               where resultado.ID == Convert.ToInt32(Console.ReadLine())
                               select resultado).FirstOrDefault<Tutoria>();
                seleccionarTutoria.Fecha = tutoria.Fecha;
                seleccionarTutoria.Aula = tutoria.Aula;
                seleccionarTutoria.Docente = tutoria.Docente;
                seleccionarTutoria.Asignatura =  tutoria.Asignatura;
                seleccionarTutoria.Tema = tutoria.Tema;
                seleccionarTutoria.Estudiante = alumnos.Nombre;

                db.Add(seleccionarTutoria);
                db.SaveChanges();

            }
        }


       
        static void LoginGeneral()
        {
            using (var db = new EntityFrameworkContext())
            {
                string contrasenia = "";
                string correo = "";
                int opcion4, opcion5, opcion6;

                Alumnos alumnos = new Alumnos();
                Docentes docentes = new Docentes();
                Administrador administrador = new Administrador();
                Console.WriteLine("Escriba el Correo de Usuario ");
                correo = Console.ReadLine();
                var admin = (from resultado in db.Administradors
                             where resultado.Correo == correo
                             select resultado).FirstOrDefault();
                Console.WriteLine("Escriba la contraseña del usuario");
                contrasenia = OcultarContrasenia();
               
                if (admin == null)
                {
                    var docente = (from resultado in db.Docentess
                                 where resultado.Correo == correo
                                 select resultado).FirstOrDefault();
                    if (docente==null)
                    {
                        var alumno = (from resultado in db.Alumnoss
                                     where resultado.Correo == correo
                                     select resultado).FirstOrDefault();
                        if (alumno == null)
                        {
                            Console.WriteLine("no existe usuario");
                        }
                        else
                        {
                            if (alumno.Contrasenia==contrasenia )

                            {
                                //metodos del estudiante 
                                Console.WriteLine("\n Bienvenido {0} {1} {2} ", alumnos.Apellido, alumnos.Nombre, alumnos.Cedula);
                                do
                                {
                                    Console.WriteLine("****************************************");
                                    Console.WriteLine("\n 1.- Listas de Tutorias Disponibles" +
                                                      "\n 2.- Seleccionar Tutoria/s" +
                                                      "\n 3.- Salir");
                                    opcion5 = Convert.ToInt32(Console.ReadLine());
                                    switch (opcion5)
                                    {
                                        case 1:
                                            Console.Clear();
                                            Console.WriteLine("*************************  LISTAS DE TUTORIAS \n ************************* ");
                                            ListaDeTutoriaestudiante();

                                            Console.WriteLine("Presione la tecla ENTER para continuar");

                                            break;
                                        case 2:
                                            Console.WriteLine("*************************  SELECCIONAR TUTORIAS \n ************************* ");
                                            ListaDeTutoriaestudiante();
                                            // SeleccionarTutoria();
                                            SeleccionarTutoria seleccionarTutoria = new SeleccionarTutoria();
                                            Console.WriteLine("Seleccione la tutoria a la que se quiee integrar");
                                            var tutoria = (from resultado in db.Tutorias
                                                           where resultado.ID == Convert.ToInt32(Console.ReadLine())
                                                           select resultado).FirstOrDefault<Tutoria>();
                                            seleccionarTutoria.Fecha = tutoria.Fecha;
                                            seleccionarTutoria.Aula = tutoria.Aula;
                                            seleccionarTutoria.Docente = tutoria.Docente;
                                            seleccionarTutoria.Asignatura = tutoria.Asignatura;
                                            seleccionarTutoria.Tema = tutoria.Tema;
                                            seleccionarTutoria.Estudiante = alumnos.Nombre;

                                            db.Add(seleccionarTutoria);
                                            db.SaveChanges();


                                            Console.WriteLine("Presione la tecla ENTER para continuar");

                                            break;
                                        case 3:
                                            break;
                                    }
                                } while (opcion5 != 3);
                            }
                            else
                            {
                                Console.WriteLine("Contraseña incorrecta");
                                LoginGeneral();
                            }
                        }
                    }
                    else
                    {
                        if (docente.Contrasenia == contrasenia )
                        {
                            //metodos del docente 
                            Console.WriteLine("\n Bienvenido {0} {1} {2} ", docente.Apellido, docente.Nombre, docente.Cedula);
                            do
                            {
                                Console.WriteLine("****************************************");
                                Console.WriteLine("\n 1.- Crear Tutoría" +
                                                  "\n 2.- Ver Tutorias Creadas" +
                                                  "\n 3.- Salir");
                                opcion4 = Convert.ToInt32(Console.ReadLine());
                                switch (opcion4)
                                {
                                    case 1:
                                        Tutoria tutoria = new Tutoria();
                                        DateTime fecha = DateTime.Now;
                                        Console.WriteLine("Proceda a escribir la fecha y hora de la Tutoria");
                                        Console.WriteLine("Escriba el Año ");
                                        string anio = Console.ReadLine();
                                        Console.WriteLine("Escriba el Mes");
                                        string mes = Console.ReadLine();
                                        Console.WriteLine("Escriba el Dia");
                                        string dia = Console.ReadLine();
                                        Console.WriteLine("Escriba la hora");
                                        string hora = Console.ReadLine();
                                        DateTime crearfecha = new DateTime(int.Parse(anio), int.Parse(mes), int.Parse(dia), int.Parse(hora), 0, 0);
                                        tutoria.Fecha = crearfecha;

                                        Console.WriteLine("Digite el Aula a utilizar");
                                        ListaAulas();
                                        var aulas = (from resultado in db.Aulas
                                                     where resultado.Id == Convert.ToInt32(Console.ReadLine())
                                                     select resultado).FirstOrDefault<Aula>();
                                        tutoria.Aula = aulas.NombreAula;
                                        Console.WriteLine("Ingrese la Asignatura");
                                        tutoria.Asignatura = Console.ReadLine();
                                        Console.WriteLine("Ingrese el Tema");
                                        tutoria.Tema = Console.ReadLine();
                                        Console.WriteLine("\nSe asignó tutoría para:");
                                        tutoria.FechaCreacion = DateTime.Now;
                                        tutoria.Docente = docente.Nombre + docente.Apellido;
                                        Console.WriteLine(tutoria.Fecha + " " + tutoria.Aula + " " + tutoria.Asignatura + "\n" + tutoria.Tema + "    Fecha de Creacion " + tutoria.FechaCreacion);

                                        db.Add(tutoria);
                                        db.SaveChanges();


                                        Console.WriteLine("Presione la tecla ENTER para continuar");

                                        break;
                                    case 2:
                                        ListaDeTutorias();
                                      
                                        Console.WriteLine("Presione la tecla ENTER para continuar");

                                        break;
                                    case 3:
                                        break;
                                }
                            } while (opcion4 != 3);
                        }
                        else
                        {
                            Console.WriteLine("Contraseña incorrecta");
                            LoginGeneral();
                        }
                    }

                }
                else
                {
                    if (admin.Contrasenia == contrasenia )
                    {
                        //metodos del aadminstrador 
                        Console.WriteLine("\n Bienvenido {0} {1} {2} ", administrador.Apellido, administrador.Nombre, administrador.Cedula);
                        do
                        {
                            Console.WriteLine("****************************************");
                            Console.WriteLine("\n 1.- Ingresar Aulas" +
                                              "\n 2.- Listas de Aulas" +
                                              "\n 3.- Seleccione la Tutoría a Visualizar" +
                                              "\n 4.- Listas de Docentes" +
                                              "\n 5.- Listas de Alumnos" +
                                              "\n 6.- Salir");
                            opcion6 = Convert.ToInt32(Console.ReadLine());
                            switch (opcion6)
                            {

                                case 1:
                                    IngresarAulas();
                                    Console.WriteLine("Presione la tecla ENTER para continuar");
                                    break;
                                case 2:
                                    ListaAulas();
                                    Console.WriteLine("Presione la tecla ENTER para continuar");
                                    break;
                                case 3:
                                    SeleccionarTutoria seleccionarTutoria = new SeleccionarTutoria();
                                    ListaDeTutorias();
                                  
                                    Console.WriteLine("Presione la tecla ENTER para continuar");
                                    break;
                                case 4:
                                    ListadoProfesores();
                                    Console.WriteLine("Presione la tecla ENTER para continuar");
                                    break;
                                case 5:
                                    ListadoAlumnos();
                                  
                                    Console.WriteLine("Presione la tecla ENTER para continuar");
                                    break;
                                case 6:
                                    break;
                            }
                        } while (opcion6 != 6);
                    }
                    else
                    {
                        Console.WriteLine("Contraseña incorrecta");
                        LoginGeneral();
                    }
                }
                

            }
        }
       static string OcultarContrasenia()
        {
            ConsoleKeyInfo ultimaTecla;
            bool continuar;
            char mostrar;
            char[] cadena = new char[100];
            int i = 0;
            continuar = true;
            mostrar = '*';
            string contraseniaIngresada = "";
            while (continuar)
            {

                ultimaTecla = Console.ReadKey(true);

                if (ultimaTecla.KeyChar != 13)
                {

                    if (ultimaTecla.KeyChar != 8)
                    {
                        cadena[i] = ultimaTecla.KeyChar;
                        i++;
                        Console.Write(mostrar);
                    }
                    else
                    {
                        cadena[i] = '\0';
                        i--;
                        Console.Write("\b \b");
                        continuar = true;
                    }

                }
                else
                    continuar = false;
            }
            cadena[i] = '\0';

            for (int j = 0; j < cadena.Length; j++)
            {
                if (cadena[j].Equals('\0'))
                {
                    break;
                }
                else
                {
                    contraseniaIngresada = contraseniaIngresada + cadena[j];
                }
            }
            return contraseniaIngresada;
        }

    }
}

